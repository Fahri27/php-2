<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Berlatih Looping</title>
</head>
<body>
    <h1> Berlatih Looping </h1>
    <?php
    echo "<h3> Soal No 1 Looping I Love PHP </h3>";
    echo "<h4> Looping Pertama </h4>";
    for($i=2; $i<=20; $i+=2){
        echo $i . " - Angka Genap <br>";
    }
    echo "<h4> Looping Kedua </h4>";
    for($a=20; $a>=2; $a-=2){
        echo $a . " - Angka Genap <br>";
    }

    echo "<h3> Soal No 2 Looping Array</h3>";
    $number = [18, 45, 29, 61, 47, 34];
    echo "array number : ";
    echo "<br>";
    print_r($number);
    echo "<br>";
    echo "Array sisa hasil baginya adalah: ";
    echo "<br>";
    foreach($number as $value) {
        $rest[] = $value %5;
    }
    print_r($rest);
    echo "<br>";

    echo "<h3> Soal No 3 Looping Asociative Array</h3>";
    $produk = [
        ["Keyboard Logitek",60000,"Keyboard yang mantap untuk kantoran","logitek.jpeg"],
        ["Keyboard MSI",300000,"Keyboard gaming MSI mekanik","msi.jpeg"],
        ["Mouse Genius",50000,"Mouse Genius biar lebih pinter","genius.jpeg"],
        ["Mouse Jerry",30000,"Mouse yang disukai kucing","jerry.jpeg"]
    ];
    foreach($produk as $key => $value){
        $item = array(
            'name' => $value[0],
            'price' => $value[1],
            'description' => $value[2],
            'source' => $value[3]
        );
        print_r($item);
        echo "<br>"; 
    }

    echo "<h3>Soal No 4 Asterix </h3>";
    for($j=1; $j<=5; $j++){
        for($b=1; $b<=$j; $b++){
            echo "*";
        }
        echo "<br>";
    }
    ?>
</body>
</html>
